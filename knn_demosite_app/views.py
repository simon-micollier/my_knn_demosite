'''
author : simon.micollier@gmail.com
creation date : 19/10/2015
goals :
    define a user UI
    manipulate KNN-algorithm objects/functions in main()
    CSV import reference dataset
next :
    implement objects persistance in django DB e.g: reference dataset,
    feature definition, feature index loaded from DB
'''
# GENERAL imports
import csv
import copy
import json, requests
import time
import os

# DJANGO imports
from django.template import Context, RequestContext
from django.shortcuts import render_to_response, render
# MY CUSTOM models
from knn_demosite_app.models import UIUnknownNodeForm, ReferenceDataset,ReferenceFeature, ReferenceNode
# MY CUSTOM modules
from knn_demosite_app.knn_algo import Feature, Node, Nodelist
from django.conf import settings
from algoliasearch import algoliasearch

referenceDSnodes = []

apidomain = 'http://api.deezer.com'
DzrChartsReq = apidomain + '/chart/0/artists/'
DzrArtistReq = apidomain + '/artist/'
DzrSearchReq = apidomain + '/search/artist/'

referenceCSVpath = settings.BASE_DIR+'/knn_demosite_app/static/simon_dataset.csv'
querysetCSVpath = settings.BASE_DIR+'/knn_demosite_app/static/simon_queryset.csv'

#referenceCSVpath = '/knn_demosite_app/static/simon_dataset.csv'
#querysetCSVpath = '/knn_demosite_app/static/simon_queryset.csv'

# ----------- Homepage view
def homeview(request):
    form = UIUnknownNodeForm()
    # client = algoliasearch.Client("H9GX2VHF2G","40a28ac98de357bd211066cea98ab6df")
    # res = searchAlgolia(request,client)
    if request.method == 'POST':
        form = UIUnknownNodeForm(request.POST)  # , request.FILES
        if (request.POST['dim_1'] and
            request.POST['dim_2']):
            dim_1 = request.POST['dim_1']
            spec_1 = int(request.POST['spec_1'])
            dim_2 = request.POST['dim_2']
            spec_2 = int(request.POST['spec_2'])
            # call knn main
            result = mainKNNalgo({dim_1: spec_1, dim_2: spec_2})
            return render_to_response('success.html', {'reportviews': result,'res' : '0'}, context_instance=RequestContext(request))
    return render(request, 'index.html', {'res' : '0'})


def searchAlgolia(request,client):
    index = client.init_index("getstarted_actors")
    res = index.search("portm", { "attributesToRetrieve": "fistname,lastname", "hitsPerPage": 20})
    #res = "test"
    return res

# ----------- Main of KNN-ALGO which return the result of the KNN into a dictionnary (used for template success.html)
def mainKNNalgo(user_node_toclassify):

    # create KNN feature/dimension objects if feature/dimension do not exist in the application
    knnfeatures = {}
    for user_dim in user_node_toclassify:
        # @todo if !(feature in app):
        knnfeatures.update({user_dim: Feature(user_dim)})

    # create a reference dataset
    # @todo verifier que ca marche vu que les features des autres noeuds ne réfère pas à une feature unique
    referenceDSnodes = createNodelistFromCSV(settings.BASE_DIR + '/knn_demosite_app/static/simon_dataset.csv')

    # create a node with an unknown type
    notype_node = Node('toclassify')
    for user_dim_it in user_node_toclassify:
        # add to this node a dict entry of type feature-value
        # I choosed dict structure to benefict of keys() values() access on the features_spec index
        notype_node.add_feature_spec({knnfeatures[user_dim_it]: user_node_toclassify[user_dim_it]})

    referenceDSnodes.append(notype_node)  # add it to the TEST dataset

    # create a Nodelist which is the full dataset used as only input of KNN-algo
    housing_heritage = Nodelist(referenceDSnodes, 3)  # 3 is the K-max neighbor parameter

    # refresh feature weights based on TEST dataset
    for feature in knnfeatures:
        knnfeatures[feature].refreshfeatureWeight(housing_heritage)

    print(housing_heritage.debug_nodelist())
    # run KNN-algo with parameter subset/fullset of features, and list of type to guess
    return housing_heritage.determineUnknown(knnfeatures.values(), ['official', 'duplicate'])


# Create NodeList class Reference DATASET from knn_algo from a CSV file
def createNodelistFromCSV(path):
    referenceDSnodes = []
    with open(path) as csvfile:
        reader = csv.DictReader(csvfile, ['type', 'feature1', 'feature2', 'spec1', 'spec2'])
        rownum = 0
        for row in reader:
            if rownum != 0:
                currentnode = Node(row['type'])
                currentnode.add_feature_spec({row['feature1']: int(row['spec1'])})
                currentnode.add_feature_spec({row['feature2']: int(row['spec2'])})
                referenceDSnodes.append(currentnode)
            rownum += 1
        return referenceDSnodes


# Create famous artists REFERENCE dataSET from Deezer charts
# @output : csv file for persistance @todo : connexion with Database
def createDzrFamousArtists(path, DzrChartsReq='', DzrArtistReq=''):
    params = dict(
        index='0',
        limit='100', # TOP 100 artists
        output='json'
        )
    response_chart = requests.get(url=DzrChartsReq, params=params)
    json_topArtist = json.loads(response_chart.text)
    # print(json_topArtist)
    with open(path, 'w') as csvfile:
        fieldnames = ['type', 'feature1', 'feature2', 'spec1', 'spec2','artist_id']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for item in json_topArtist['data']:
            artist_id = item.get("id")
            artistReq = DzrArtistReq + str(artist_id)
            print(artistReq)
            response_artist = requests.get(url=artistReq)
            json_artist = json.loads(response_artist.text)
            time.sleep(0.2) # delays in seconds
            writer.writerow({'type': 'official','feature1': 'nb_album','feature2': 'nb_fan', 'spec1': json_artist.get('nb_album'),'spec2': json_artist.get('nb_fan'), 'artist_id': artist_id})


'''
def readDzrFamousArtists(path='knn_demosite_app/static/simon_dataset.csv'):
    with open(path) as csvfile:
        fieldnames = ['type', 'feature1', 'feature2', 'spec1', 'spec2','artist_id']
        reader = csv.DictReader(csvfile, fieldnames=fieldnames)
        # reader.readheader()
        rownum = 0
        for row in reader:
            if rownum != 0:

            rownum += 1
'''


# @ Deezer suspicious ALGO steps :
# 1) create a suspicious queryset of duplicated Deezer artists
# 2) send suspicious queryset to knn algorithm for ML Decision making
def createDzrSuspiciousArtists(path, DzrSearchReq='', DzrChartsReq=''):
    suspicious_artist_id=[]
    params = dict(
        index='0',
        limit='100',
        output='json'
        )
    resp = requests.get(url=DzrChartsReq, params=params)
    res_json_obj = json.loads(resp.text)
    with open(path, 'w') as csvfile:
        fieldnames = ['name', 'nb_album', 'nb_fan', 'radio', 'picture_small']
        # dimensions for Machine Learning decision making
        # blackbox decision maker but we think it will works because
        # 1 fake artist usually have nb_album low
        # 2 fake artist usually have nb_fan low
        # 3 fake artist usually have No or Black picture / check picture size
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        # foreach TOP famous artist...
        for item in res_json_obj['data']:
            artist_name = item.get("name")
            params_search = dict(
                index='0',
                limit='50',
                output='json',
                q=artist_name
            )
            resp_search = requests.get(url=DzrSearchReq, params=params_search)
            time.sleep(0.1) # delays in seconds
            res_search_json = json.loads(resp_search.text)
            # ... we search if this artist have some duplicates :
            for res_artist in res_search_json['data']:
                res_name = res_artist.get('name')
                # condition : we suppose an artist is suspicious if duplicate
                if (artist_name==res_name and int(res_artist.get('nb_fan'))<10000 ):
                    # @TODO picture dimension calculate size of the picture
                    writer.writerow({'name': res_name, 'nb_album': res_artist.get('nb_album'), 'nb_fan': res_artist.get('nb_fan'), 'radio': res_artist.get('radio'), 'picture_small': res_artist.get('picture_small')})


def readDzrSuspiciousArtists(querysetCSVpath):
    print(os.getcwd())
    print(settings.BASE_DIR)
    currentcwd=os.getcwd()
    suspicious=[]
    with open(querysetCSVpath) as csvfile:
        fieldnames = ['name', 'nb_album', 'nb_fan', 'radio', 'picture_small']
        reader = csv.DictReader(csvfile, fieldnames)
        rownum = 0
        for row in reader:
            if rownum != 0:
                 content = (list(row[it] for it in fieldnames))
                 suspicious.append(content)
            rownum += 1
        print(suspicious)
        return suspicious



# createDzrFamousArtists('knn_demosite_app/static/simon_dataset.csv', DzrChartsReq, DzrArtistReq)

# createDzrSuspiciousArtists('knn_demosite_app/static/simon_queryset.csv', DzrSearchReq, DzrChartsReq)


def runDecisionMaking(querysetCSVpath,referenceCSVpath):

    # create KNN features from CSV file
    knnfeatures = {}
    # @todo check all features in CSV file and create them
    knnfeatures.update({"nb_album": Feature("nb_album")})
    knnfeatures.update({"nb_fan": Feature("nb_fan")})

    # foreach suspicious/duplicate artist from query set :
    with open(querysetCSVpath) as csvfile:
        print('preparation')
        # data preparation
        fieldnames = ['name', 'nb_album', 'nb_fan', 'radio', 'picture_small']
        reader = csv.DictReader(csvfile, fieldnames)
        rownum = 0
        for row in reader:
            referenceDSnodes = createNodelistFromCSV(referenceCSVpath)
            if rownum != 0:
                notype_node = Node('toclassify') # create node to classify
                # check feature name # @ TODO for all dimensions
                if (fieldnames[1] in knnfeatures.keys()):
                    notype_node.add_feature_spec({knnfeatures['nb_album']: int(row['nb_album'])})
                if (fieldnames[2] in knnfeatures.keys()):
                    notype_node.add_feature_spec({knnfeatures['nb_fan']: int(row['nb_fan'])})
                referenceDSnodes.append(notype_node)
                queryknn = Nodelist(referenceDSnodes, 3)
                # update feature weight depending on nodelist
                for feature in knnfeatures:
                    knnfeatures[feature].refreshfeatureWeight(queryknn)
                queryknn.determineUnknown(knnfeatures.values(), ['official', 'duplicate'])
            rownum += 1

# runDecisionMaking(querysetCSVpath,referenceCSVpath)





