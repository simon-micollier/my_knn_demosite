from django.db import models
from django import forms
from django.forms import ModelForm

# should be filled depending on API fields dimensions
FEATURE_CHOICES = (
    ('nb_album', 'nb_album'),
    ('nb_fan', 'nb_fan')
)


# UI test node for Type guess
class UIUnknownNode(models.Model):

    dim_1 = models.CharField(max_length=200, choices=FEATURE_CHOICES,default='nb_album')
    spec_1 = models.IntegerField(default=0)
    dim_2 = models.CharField(max_length=200, choices=FEATURE_CHOICES, default='nb_fan')
    spec_2 = models.IntegerField(default=0)


class UIUnknownNodeForm(ModelForm):

    class Meta:

        model = UIUnknownNode
        fields = ['dim_1', 'spec_1', 'dim_2','spec_2']


# EVOLUTION : Reference Nodes DATASET to store in DJANGO-SQLITE
class ReferenceNode(models.Model):

    ref_node_type = models.CharField(max_length=200, default='toclassify')


class ReferenceFeature(models.Model):

    ref_node_feature_title = models.CharField(max_length=200, choices=FEATURE_CHOICES)
    ref_node_spec = models.IntegerField(default=0)
    node_id = models.ForeignKey(ReferenceNode)


class ReferenceDataset(forms.Form):

    reference_dataset_name = forms.CharField(max_length=200)
    reference_dataset_csv = forms.CharField(max_length=200)
